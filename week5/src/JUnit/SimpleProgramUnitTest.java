package JUnit;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class SimpleProgramUnitTest {

    @Test
    public void testNoVeggies() {
        //test out the assertNull function
        Assert.assertNull(SimpleProgram.NoVeggies);
    }

    @Test
    public void testRandomFood() {
        //test out the assertArrayEquals and assertNotNull functions
        String food = SimpleProgram.RandomFood();
        String[] expectedFoods = {"Hamburger","Hot Dog","Tacos","Gyros","Pizza","Pasta","Fried Rice"};
        Assert.assertArrayEquals(expectedFoods,SimpleProgram.foods);
        Assert.assertNotNull(food);
        Assert.assertThat(SimpleProgram.foods[0], is("Hamburger"));
    }

    @Test
    public void testSubtractAndMultiply() {
        //test out the assertNotSame, assertSame, assertEquals
        double mul8and7dub = SimpleProgram.MultiplyDub(8,7);
        int mul8and7int = SimpleProgram.MultiplyInt(8,7);
        Assert.assertNotSame("These are the same value.",mul8and7int,mul8and7dub);
        double int2dub = Double.valueOf(mul8and7int);
        Assert.assertEquals(int2dub,mul8and7dub, 0);
        //The assert below would fail because it only allows primitives. So I converted to int.
        //Assert.assertSame(int2dub,mul8and7int);
        int dub2int1 = (int)mul8and7dub;
        int dub2int2 = (int)int2dub;
        Assert.assertSame(dub2int1,dub2int2);
    }

    @Test
    public void MoreSubtractMulitplyTesing() {
        //this method uses the assertFalse, assertTrue functions
        int sub6minus2 = SimpleProgram.SubtractInt(6,2);
        int mul2times2 = SimpleProgram.MultiplyInt(2,2);
        Assert.assertTrue("Isn't a true statement",sub6minus2 == mul2times2);
        Assert.assertFalse("Isn't a false statement",(sub6minus2 / 2) == mul2times2);
    }

}
