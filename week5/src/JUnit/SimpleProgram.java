package JUnit;

import java.util.ArrayList;
import java.util.Random;

public class SimpleProgram {

    public static String NoVeggies;
    public static String[] foods = {"Hamburger","Hot Dog","Tacos","Gyros","Pizza","Pasta","Fried Rice"};

    public static void main(String[] args) {
        System.out.println(RandomFood());
    }

    public static String RandomFood() {
        String food;
        Random random = new Random();
        int randomInt = random.nextInt(foods.length);
        food = foods[randomInt];
        return food;
    }

    public static double MultiplyDub(double num1, double num2) {
        return num1 * num2;
    }
    public static double SubtractDub(double num1, double num2) {
        return num1 - num2;
    }

    public static int MultiplyInt(int num1, int num2) {
        return num1 * num2;
    }

    public static int SubtractInt(int num1, int num2) {
        return num1 - num2;
    }
}
