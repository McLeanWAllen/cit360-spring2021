/*
 * filename: w2Collections.java
 * author: Mac Allen
 * date work began: 29 April 2020
 */

package Collections;

import java.lang.*;
import java.util.*;
import java.util.List;

public class w2Collections
{
    public static void main(String[] args)
    {
        //Here is the list part of the assignment. I used ArrayList.
        List<String> statementList = new LinkedList<>();
        statementList.add("Bigfoot");
        statementList.add("goes by many names");
        statementList.add("such as:");
        statementList.add("Sasquatch");
        statementList.add("Yeti");
        statementList.add("and the");
        statementList.add("Abominable Snowman");
        statementList.add("My favorite of them all is");
        statementList.add("Bigfoot");

        System.out.println("........List........");
        for (int i = 0; i < statementList.size(); i++)
        {
            System.out.println(statementList.get(i));
        }

        //Here is the queue part of the assignment. I used PriorityQueue.
        Queue<String> statementQueue = new PriorityQueue<>();
        statementQueue.add("Bigfoot");
        statementQueue.add("goes by many names");
        statementQueue.add("such as:");
        statementQueue.add("Sasquatch");
        statementQueue.add("Yeti");
        statementQueue.add("and the");
        statementQueue.add("Abominable Snowman");
        statementQueue.add("My favorite of them all is");
        statementQueue.add("Bigfoot");

        System.out.println("\n........Queue........");
        Iterator<String> iterator = statementQueue.iterator();
        while (iterator.hasNext())
        {
            System.out.println(statementQueue.poll());
        }


        //This is the Tree part of the assignment.
        Set<String> statementTree = new TreeSet<>();
        statementTree.add("Bigfoot");
        statementTree.add("goes by many names");
        statementTree.add("such as:");
        statementTree.add("Sasquatch");
        statementTree.add("Yeti");
        statementTree.add("and the");
        statementTree.add("Abominable Snowman");
        statementTree.add("My favorite of them all is");
        statementTree.add("Bigfoot");

        System.out.println("\n........Tree........");
        for (String s : statementTree) {
            System.out.println(s);
        }

        //This is the set part of the assignment.
        Set<String> statementSet = new HashSet<>();
        statementSet.add("Bigfoot");
        statementSet.add("goes by many names");
        statementSet.add("such as:");
        statementSet.add("Sasquatch");
        statementSet.add("Yeti");
        statementSet.add("and the");
        statementSet.add("Abominable Snowman");
        statementSet.add("My favorite of them all is");
        statementSet.add("Bigfoot");

        System.out.println("\n........Set........");
        Iterator<String> j = statementSet.iterator();
        while (j.hasNext())
        {
            System.out.println(j.next());
        }

        List<Bigfeet> bigfootList = new ArrayList<>();
        bigfootList.add(new Bigfeet("Bigfoot",
                "All around the world",
                "International Hide and Seek Champion"));
        bigfootList.add(new Bigfeet("Sasquatch",
                "North America",
                "Those folks in Saskatchewan come close to getting me every year!"));
        bigfootList.add(new Bigfeet("Yeti",
                "South-Central Asia (the Himilayas)",
                "Heavyweight Bigfoot MMA Title holder"));
        bigfootList.add(new Bigfeet("Abominable Snowman",
                "North American Folklore",
                "The only one in the bigfoot collection that is not real!"));

        System.out.println("\n........List using Generics........");
        for (int t = 0; t < bigfootList.size(); t++)
        {
            System.out.println(bigfootList.get(t));
        }
    }
}
