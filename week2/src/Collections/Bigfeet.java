package Collections;

import java.text.MessageFormat;

public class Bigfeet
{
    private final String bigfootName;
    private final String bigfootRegion;
    private final String bigfootClaim;

    public Bigfeet(String bigfootName, String bigfootRegion, String bigfootClaim)
    {
        this.bigfootName   = bigfootName;
        this.bigfootRegion = bigfootRegion;
        this.bigfootClaim  = bigfootClaim;
    }

    public String toString()
    {
        return "Name of species: %s\nRegion where they live: %s\nClaim to fame: %s\n".formatted(bigfootName,
                bigfootRegion, bigfootClaim);
    }
}
