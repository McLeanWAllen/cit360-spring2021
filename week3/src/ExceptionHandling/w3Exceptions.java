/*
 * filename: w3Exceptions.java
 * author: Mac Allen
 * date work began: 5 May 2020
 */
package ExceptionHandling;

import java.lang.*;
import java.util.*;

public class w3Exceptions {
    public static void main(String[] args)
    {
        //initialization of needed variables.
        Scanner input = new Scanner(System.in);
        String inputNum1;
        String inputNum2;

        System.out.println("Please enter two numbers.\n" +
                "We will divide the first by the second.");

        //This is where we get inputs and ensure they can be converted to doubles.
        System.out.print("Enter the first number: ");
        inputNum1 = input.nextLine();
        if (!(inputNum1.matches("\\d*[.]\\d+") | inputNum1.matches("\\d+")))
        {
            System.out.print("Please enter number [1] using digits: ");
            inputNum1 = input.nextLine();
        }
        System.out.print("Enter the second number: ");
        inputNum2 = input.nextLine();
        if (!(inputNum2.matches("\\d*[.]\\d+") | inputNum2.matches("\\d+")))
        {
            System.out.print("Please enter number [2] using digits: ");
            inputNum2 = input.nextLine();
        }

        //Conversion of strings to doubles.
        double num1 = Double.parseDouble(inputNum1);
        double num2 = Double.parseDouble(inputNum2);

        //Runs the division and looks for a divisor of zero.
        while(true)
        {
            try
            {
                System.out.printf("The quotient is: %.2f", divide2Nums(num1, num2));
                break;
            }
            catch (ArithmeticException aE)
            {
                System.out.println(aE.getMessage());
                System.out.print("Re-enter the second number (not a zero): ");
                num2 = input.nextDouble();
            }
        }
    }

    public static double divide2Nums(double num1, double num2) throws ArithmeticException
    {
        if (num2 == 0)
        {
            throw new ArithmeticException("No dividing by zero!");
        }
        return num1 / num2;
    }
}
