package JSONandHTTP;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class w4JSON
{

    public static String IPtoJSON(IPAddress ip)
    {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try
        {
            s = mapper.writeValueAsString(ip);
        }
        catch (JsonProcessingException e)
        {
            System.err.println(e);
        }

        return s;
    }

    public static IPAddress JSONtoIP(String s)
    {
        ObjectMapper mapper = new ObjectMapper();
        IPAddress ip = null;

        try
        {
            ip = mapper.readValue(s, IPAddress.class);
        }
        catch (JsonProcessingException e)
        {
            System.err.println(e);
        }

        return ip;

    }
}
