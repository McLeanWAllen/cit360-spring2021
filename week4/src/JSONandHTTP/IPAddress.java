package JSONandHTTP;

public class IPAddress {

    private String ip;

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String toString()
    {
        return "Your IPv4 Address is: " + ip;
    }
}
