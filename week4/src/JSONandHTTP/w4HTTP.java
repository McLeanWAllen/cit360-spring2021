package JSONandHTTP;

import java.net.*;
import java.io.*;

public class w4HTTP
{
    public static String getHttpContent(String string)
    {
        String content="";
        try
        {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
            {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();
        }
        catch (Exception e)
        {
            System.err.println(e);
        }
        return content;
    }
}
